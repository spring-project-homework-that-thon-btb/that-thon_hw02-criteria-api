package home.com.home002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Home002Application {

    public static void main(String[] args) {
        SpringApplication.run(Home002Application.class, args);
    }
}
