package home.com.home002.controller;


import home.com.home002.entities.Account;
import home.com.home002.entities.Address;
import home.com.home002.entities.Department;
import home.com.home002.entities.Employee;
import home.com.home002.enums.Gender;
import home.com.home002.repositoy.EmployeeRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepositoy employeeRepositoy;


    /**
     * 1- all employees
     * @return
     */
    @GetMapping
    private List<Employee> index(){
        return employeeRepositoy.index();
    }

    /**
     * 2- all employee by department id
     * @param id
     * @return
     */
    @GetMapping("/dept")
    private List<Employee> allEmployeeByDept(@RequestParam(defaultValue = "1",value = "id",required = false) Integer id){
        return employeeRepositoy.allEmployeeByDept(id);
    }

    /**
     * 3- All employees by Id
     * @return
     */
    @GetMapping("/select-employee")
    public List<Employee> allEmployeeById(@RequestParam(defaultValue = "1",value = "id",required = false)Integer id){
        return employeeRepositoy.allEmployeeById(id);
    }

    /**
     * 4-Sum employee’s salary group by department
     */
    @GetMapping("/selectGroupBy")
    public Map<String,Object> selectGroupby(){
        Map<String ,Object> response=new HashMap<>();
        List<Tuple> allEmp= employeeRepositoy.selectGroupBy();
        List<Object> all=new ArrayList<>();
            for (Tuple t:
                    allEmp) {
                all.add("Department_ID : "+ t.get(2));
                all.add("Department_Name : "+ t.get(1));
                all.add("Employee_Salary : "+ t.get(0));
            }
        response.put("data",all);
        return response;
    }



    /**
     * 5-Select employee id, name, gender, salary and wrap it into new object
     */
    @GetMapping("/object")
    public String allEmpyeeObject(){
        employeeRepositoy.allEmployeeObject();
        return "Please see them on console";
    }

    /**
     * 6-Select all department with number of employees in that department
     */
    @GetMapping("/department")
    public String allDepartmentWithTotalEmployee(){
        employeeRepositoy.allDepartment();
        return "please all department with number of employees on console java";
    }


    /**
     * 7- find max salary of employee
     * @return
     */
    @GetMapping("/max")
    private Double maxVal(){
        return employeeRepositoy.maxVal();
    }

    /**
     * 8- find employee between salary
     */
    @GetMapping("/salary/between")
    public List<Employee> employeeSalaryBetween(
            @RequestParam(defaultValue = "300.0",value = "min",required = false) Double min,
            @RequestParam(defaultValue = "500.0",value = "max",required = false) Double max
    ){
        return employeeRepositoy.salaryBetween(min,max);
    }

    /**
     * 9- update all employee salary in department
     */
    @GetMapping("/update/dept")
    public Integer updateEmployeeSalaryByDept(@RequestParam(defaultValue = "1",value = "id",required = false)Integer id){
        return employeeRepositoy.updateEmployeeSalaryByDepartment(id);
    }

    /**
     * 10- delelete employee by id
     */
    @GetMapping("/delete")
    public Integer deleteEmployees(@RequestParam(defaultValue = "1",value = "id",required = false)Integer id){
        return employeeRepositoy.deleteEmployee(id);
    }

    /**
     * 10- update empoyee by id
     */
    @GetMapping("update")
    public Integer updateEmployee(@RequestParam(defaultValue = "1",value = "id",required = false)Integer id){
        return employeeRepositoy.updateEmployee(id);
    }

    /**
     * add data into all table
     * @return
     */
    @GetMapping("/add")
    public Employee add(){
        Address address=new Address("st001","House 001","KPS","Cambodia");
        Employee employee=new Employee("Tola",Gender.male,address,1200.25);

        List<Employee> list=new ArrayList<>();
        list.add(employee);
        Department department=new Department();
        department.setDepartmentName("Spring Team");
        employee.setDepartment(department);
        department.setEmployees(list);
        Account account=new Account();
        account.setAccountNumber("N004");
        account.setEmployee(employee);
        employee.setAccount(account);

        employeeRepositoy.add(department);
        return employee;
    }


}
