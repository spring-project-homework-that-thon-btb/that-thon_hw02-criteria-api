package home.com.home002.repositoy;

import home.com.home002.entities.Account;
import home.com.home002.entities.Address;
import home.com.home002.entities.Department;
import home.com.home002.entities.Employee;
import home.com.home002.enums.Gender;
import org.springframework.stereotype.Repository;
import sun.rmi.runtime.Log;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class EmployeeRepositoy {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * 1- get all employees
     * @return
     */
    public List<Employee> index(){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


    /**
     * 2- all employee by department id
     * @param id
     * @return
     */
    public List<Employee> allEmployeeByDept(Integer id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        Join<Employee, Department> department=root.join("department");
        criteriaQuery.select(root).where(criteriaBuilder.equal(department.get("id"),id));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }


    /**
     * 3- all employee by employee id
     * @param id
     * @return
     */
    public List<Employee> allEmployeeById(Integer id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id"),id));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /**
     * 4-Sum employee’s salary group by department
     */

    public List<Tuple> selectGroupBy(){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createQuery(Tuple.class);
        Root<Employee> employee=criteriaQuery.from(Employee.class);
        Join<Employee, Department> department=employee.join("department");
        criteriaQuery.multiselect(criteriaBuilder.sum(employee.get("salary")),department.get("departmentName"),department.get("id")).groupBy(department.get("id"));
        List<Tuple> allEmplpoyee=entityManager.createQuery(criteriaQuery).getResultList();
//        for (Tuple t:
//                allEmplpoyee) {
//            System.out.println("Department_Id: "+t.get(1)+","+"\t"+"Total_Salary-Employee: "+t.get(0));
//        }
        return allEmplpoyee;
    }

    /**
     * 5-Select employee id, name, gender, salary and wrap it into new object
     */

    public List<Employee> allEmployeeObject(){
        List<Employee> employees=new ArrayList<>();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createQuery(Tuple.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        criteriaQuery.multiselect(root.get("id"),root.get("empName"),root.get("gender"),root.get("address"),root.get("salary"));
        List<Tuple> allEmplpoyee=entityManager.createQuery(criteriaQuery).getResultList();
        for (Tuple t:
                allEmplpoyee) {
            System.out.println("ID: "+t.get(0));
            System.out.println("Name: "+t.get(1));
            System.out.println("Gender: "+t.get(2));
            System.out.println("Address: "+t.get(3));
            System.out.println("Salary: "+t.get(4));
        }


        return null;
    }

    /**
     * 6-Select all department with number of employees in that department
     */
    public void allDepartment(){
       CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
       CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createQuery(Tuple.class);
       Root<Department> department=criteriaQuery.from(Department.class);
       Join<Department, Employee> employee=department.join("employees");
        criteriaQuery.multiselect(department.get("id"),department.get("departmentName"),criteriaBuilder.count(department.get("id"))).groupBy(department.<Employee>get("id"));
        List<Tuple> allEmplpoyee=entityManager.createQuery(criteriaQuery).getResultList();
        for (Tuple t:
                allEmplpoyee) {
            System.out.println("Department_Id: "+t.get(0)+"  ,Department_Name: "+t.get(1)+"  ,Total_Employee: "+t.get(2));
        }
    }



    /**
     * 7- find max salary
     * @return
     */
    public Double maxVal(){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> q=criteriaBuilder.createQuery(Double.class);
        Root<Employee> root=q.from(Employee.class);
        q.select(criteriaBuilder.max(root.get("salary")));
        Double e=entityManager.createQuery(q).getSingleResult();
        return e;
    }

    /**
     * 8- Find employees with salary between 300-500
     */
    public List<Employee> salaryBetween(Double min, Double max){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root=criteriaQuery.from(Employee.class);
        criteriaQuery.select(root).where(criteriaBuilder.between(root.get("salary"),min,max));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /**
     * 9- Set(update) all employees’ salary by department.
     * (Employees’ salary in that department are the same)
     */

    public Integer updateEmployeeSalaryByDepartment(Integer id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Employee> criteriaUpdate=criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root<Employee> root=criteriaUpdate.from(Employee.class);
        criteriaUpdate.set(root.get("salary"),1200);
        criteriaUpdate.where(criteriaBuilder.equal(root.get("department").get("id"),id));
        return entityManager.createQuery(criteriaUpdate).executeUpdate();
    }

    /**
     * 10- delelete employee by id
     */
    public Integer deleteEmployee(Integer id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaDelete<Employee> criteriaDelete=criteriaBuilder.createCriteriaDelete(Employee.class);
        Root<Employee> root=criteriaDelete.from(Employee.class);
        criteriaDelete.where(criteriaBuilder.equal(root.get("id"),id));
        return entityManager.createQuery(criteriaDelete).executeUpdate();
    }

    /**
     * 10- update empoyee by id
     */
    public Integer updateEmployee(Integer id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Employee> cd=criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root<Employee> root=cd.from(Employee.class);
        cd.set(root.get("empName"),"Nita");
        cd.where(criteriaBuilder.equal(root.get("id"),id));
        return entityManager.createQuery(cd).executeUpdate();
    }

    public void add(Department department){
        entityManager.persist(department);
    }



}
